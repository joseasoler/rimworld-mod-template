RimWorld Mod Template
===

[![RimWorld](https://img.shields.io/badge/RimWorld-1.3-informational)](https://rimworldgame.com/) [![License: Unlicense](https://img.shields.io/badge/license-Unlicense-blue.svg)](http://unlicense.org/) [![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](code_of_conduct.md)

RimWorld mod template configured for an easier C# modding experience. The mod is already set up for working with [Harmony](https://steamcommunity.com/workshop/filedetails/?id=2009463077).

To compile this mod template on Windows, you will need to install the [.NET Framework 4.7.2 Developer Pack](https://dotnet.microsoft.com/en-us/download/dotnet-framework/net472). On Linux the packages you need vary depending on your distribution of choice.

The mod can be compiled right after cloning the repository. No extra setup steps are necessary! It uses [RimRef](https://github.com/krafs/RimRef) instead of requiring the RimWorld DLL.

Private definitions in the RimWorld DLL can be accessed directly thanks to [Publicizer](https://github.com/krafs/Publicizer).

All dependencies are downloaded automatically by [NuGet](https://www.nuget.org/).


Contributions
---

This project encourages community involvement and contributions. Check the [CONTRIBUTING](CONTRIBUTING.md) file for details. Existing contributors can be checked in the [contributors list](https://gitlab.com/joseasoler/rimworld-mod-template/-/graphs/main).

Unlicense
---

To simplify its adoption for any purpose, this project is dedicated to the public domain. See the [UNLICENSE](UNLICENSE) file for details. Check the [Unlicense website](https://unlicense.org/) to learn more. This mod and all contributions to it must comply with the terms of the [Rimworld End-User License Agreement](https://store.steampowered.com/eula/294100_eula_1).

Acknowledgements
---

Read the [ACKNOWLEDGEMENTS](ACKNOWLEDGEMENTS.md) file for details.
