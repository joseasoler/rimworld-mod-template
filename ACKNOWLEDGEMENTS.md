# Acknowledgements

Developing a RimWorld mod is a huge task that is only made possible because its awesome community shares their expertise and knowledge with others. There are also people and groups outside of the RimWorld community that have been helpful in creating this mod.  All of them are hopefully listed below. Please create a new issue report if anyone is missing!

* **[Andreas Pardeike](https://www.patreon.com/pardeike/posts)** - [Harmony](https://harmony.pardeike.net/)
* **[krafs](https://github.com/krafs/)** - [RimRef](https://github.com/krafs/RimRef), [compilation without external dependencies or extra setup steps](https://ludeon.com/forums/index.php?topic=49914.0), [Publicizer](https://github.com/krafs/Publicizer).
* **[Nadia Eghbal](https://github.com/nayafia)** - [Contributing Guides: A Template](https://github.com/nayafia/contributing-template)
* **[RimWorld Discord](https://discord.gg/rimworld)**
* **[RimWorld Subreddit](https://www.reddit.com/r/RimWorld/)**
* **[UnlimitedHugs](https://github.com/UnlimitedHugs/)** - [HugsLib](https://github.com/UnlimitedHugs/RimworldHugsLib)
* **[Vanilla Expanded Framework](https://github.com/AndroidQuazar/VanillaExpandedFramework)**
* **[Vanilla Expanded Series community](https://www.patreon.com/oskarpotocki)**
* **[Vicky Brasseur](https://www.vmbrasseur.com)** - [Forge your future with Open Source](https://pragprog.com/titles/vbopens/forge-your-future-with-open-source/)
