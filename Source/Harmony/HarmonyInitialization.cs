﻿using System.Reflection;
using Verse;

namespace RimWorldModTemplate.Harmony
{
	/// <summary>
	/// Initialization of the Harmony patching of the mod.
	/// </summary>
	[StaticConstructorOnStartup]
	public class HarmonyInitialization
	{
		/// <summary>
		/// Initialization of the Harmony patching of the mod.
		/// </summary>
		static HarmonyInitialization()
		{
			var harmony = new HarmonyLib.Harmony("ToDo.packageId");
			harmony.PatchAll(Assembly.GetExecutingAssembly());
			Log.Message("RimWorld Mod Template loaded!");
		}
	}
}